<?php 
require "header.php";

require_once 'includes/db.inc.php';
?>
    <!--  BEGIN CONTENT AREA  -->
    <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="page-header">
                    <div class="page-title">
                        <h3>Pasūtījums Nr. #<?php echo $_GET['order_id'] ? str_pad($_GET['order_id'], 5 , '0', STR_PAD_LEFT) : '#'?></h3>
                    </div>
                </div>

                <div class="row invoice layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="doc-container">
                            <div class="invoice-container">
                                <div class="invoice-inbox">
                                    <div class="invoice-header-section"  style="display: flex;">
                                        <h4 class="inv-number"><?php echo '#'.str_pad($_GET['order_id'], 5 , '0', STR_PAD_LEFT); ?></h4>
                                        <div class="invoice-action">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-printer action-print" data-toggle="tooltip" data-placement="top" data-original-title="Printēt"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
                                        </div>
                                    </div>
                                    <?php
                                        $subtotal = 0.00;
                                        $tax = 0.00;
                                        $grandtotal = 0.00;
                                        echo '<div class="invoice-'.$_GET['order_id'].'">';
                                        echo '<div class="content-section  animated animatedFadeInUp fadeInUp">

                                        <div class="row inv--head-section">

                                            <div class="col-sm-6 col-12">
                                                <h3 class="in-heading">PASŪTĪJUMS</h3>
                                                </div>
                                                    <div class="col-sm-6 col-12 align-self-center text-sm-right">
                                                        <div class="company-info">
                                                            <img src="/assets/img/logo.png" height="24" style="display: block;margin-right: 10px; width: auto;"/>
                                                            <h5 class="inv-brand-name">KAFEJNĪCA ZĪDS</h5>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                                <div class="row inv--detail-section">

                                                    <div class="col-sm-7 align-self-center">
                                                        <p class="inv-to">Pasūtītājs</p>
                                                    </div>
                                                    <div class="col-sm-5 align-self-center  text-sm-right order-sm-0 order-1">
                                                        <p class="inv-detail-title"></p>
                                                    </div>
                                                    
                                                    <div class="col-sm-7 align-self-center">
                                                        <p class="inv-customer-name">Zīds Klients</p>
                                                        <p class="inv-street-addr">Pils iela 4, Rēzekne</p>
                                                        <p class="inv"></p>
                                                    </div>
                                                    <div class="col-sm-5 align-self-center  text-sm-right order-2">
                                                        <p class="inv-list-number"><span class="inv-title">Pasūtījuma numurs : </span> <span class="inv-number">#'.str_pad($_GET['order_id'], 5 , '0', STR_PAD_LEFT).'</span></p>
                                                        <p class="inv-created-date"><span class="inv-title">Pasūtījuma datums : </span> <span class="inv-date">';
                                                        $sqlOrder = "SELECT * FROM orders WHERE id= ?";
                                                        $stmtOrder = mysqli_stmt_init($conn);
                                                        if (!mysqli_stmt_prepare($stmtOrder, $sqlOrder)) {
                                                            header('Location: /order-view.php?order_id='.$_GET['order_id'].'&error=sqlerror');
                                                            exit();
                                                        } else {
                                                            mysqli_stmt_bind_param($stmtOrder, "i", $_GET['order_id']);
                                                            mysqli_stmt_execute($stmtOrder);
                                                            $resultOrder = mysqli_stmt_get_result($stmtOrder);
                                                            $rowsOrder = mysqli_num_rows($resultOrder);
                                                            if ($rowsOrder > 0) {
                                                                if($resultOrder) {
                                                                    $rowOrder = mysqli_fetch_array($resultOrder, MYSQLI_ASSOC);
                                                                    $date = new DateTime($rowOrder['createdAt']);
                                                                } 
                                                            } else {
                                                                $date = new DateTime("2020-01-01 00:00:00");
                                                            }
                                                            echo $date->format("d.m.Y H:i");
                                                        }
                                                        echo'</span></p>
                                                        <p class="inv-due-date"><span class="inv-title">Galdiņš : </span> <span class="inv-date"> Nr. </span></p>
                                                    </div>
                                                </div>

                                                <div class="row inv--product-table-section">
                                                    <div class="col-12">
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead class="">
                                                                    <tr>
                                                                        <th scope="col">Nr.</th>
                                                                        <th scope="col">Ēdiens</th>
                                                                        <th class="text-right" scope="col">Daudzums</th>
                                                                        <th class="text-right" scope="col">Vienības cena</th>
                                                                        <th class="text-right" scope="col">Daudzums</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>';

                                                                    $sqlItems = "SELECT * FROM order_items WHERE order_id=?";

                                                                    $stmtItems = mysqli_stmt_init($conn);
                                                                
                                                                    if (!mysqli_stmt_prepare($stmtItems, $sqlItems)) {
                                                                        header('Location: /order-view.php?order_id='.$_GET['order_id'].'&error=sqlerror');
                                                                        exit();
                                                                    } else {
                                                                        mysqli_stmt_bind_param($stmtItems, "i", $_GET['order_id']);
                                                                        mysqli_stmt_execute($stmtItems);
                                                                        $resultItems = mysqli_stmt_get_result($stmtItems);
                                                                        $rowsItems = mysqli_num_rows($resultItems);
                                                                        if ($rowsItems > 0) {
                                                                            if($resultItems) {
                                                                                $rowsItems = mysqli_num_rows($resultItems);
    
                                                                                for($i = 0; $i < $rowsItems; ++$i) {
                                                                                    $int = $i + 1;
                                                                                    $rowItem = mysqli_fetch_array($resultItems, MYSQLI_ASSOC);
                                                                                    echo '<tr>';
                                                                                        echo '<td>'.$int.'</td>';
    
                                                                                        $sqlMenu = "SELECT * FROM menu WHERE id=?";
                                                                                        $stmtMenu = mysqli_stmt_init($conn); 
    
                                                                                        if (!mysqli_stmt_prepare($stmtMenu, $sqlMenu)) {
                                                                                            header('Location: /order-view.php?order_id='.$_GET['order_id'].'&error=sqlerror');
                                                                                            exit();
                                                                                        } else {
                                                                                            mysqli_stmt_bind_param($stmtMenu, "i", $rowItem['menu_id']);
                                                                                            mysqli_stmt_execute($stmtMenu);
                                                                                            $resultMenu = mysqli_stmt_get_result($stmtMenu);
                                                                                            $rowsMenu = mysqli_num_rows($resultMenu);
                                                                                            if ($rowsMenu > 0) {
                                                                                                if ($resultMenu) {
                                                                                                    $rowMenu = mysqli_fetch_array($resultMenu, MYSQLI_ASSOC);
                                                                                                    echo '<td>'.$rowMenu['name'].'</td>';
                                                                                                    echo '<td class="text-right">'.$rowItem['amount'].'</td>';
                                                                                                    echo '<td class="text-right">'.$rowMenu['price'].' €</td>';
                                                                                                    $totals = $rowMenu['price'] * $rowItem['amount'];
                                                                                                    $grandtotal = $grandtotal + $totals;
                                                                                                    echo '<td class="text-right">'.number_format($totals, 2).' €</td>';
                                                                                                }
                                                                                            } else {
                                                                                                echo '<td> Nav datu </td>';
                                                                                            }
                                                                                        }
                                                                                    echo '</tr>';
                                                                                }
                                                                            }
                                                                        }  
                                                                    }
                                                                echo '</tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mt-4">
                                                    <div class="col-sm-5 col-12 order-sm-0 order-1">
                                                    </div>
                                                    <div class="col-sm-7 col-12 order-sm-1 order-0">
                                                        <div class="inv--total-amounts text-sm-right">
                                                            <div class="row">
                                                                <div class="col-sm-8 col-7">
                                                                    <p class="">Starpsumma: </p>
                                                                </div>
                                                                <div class="col-sm-4 col-5">
                                                                    <p class="">';
                                                                    $subtotal = $grandtotal * ((100-21) / 100);
                                                                    echo number_format($subtotal, 2);
                                                                    echo ' €</p>
                                                                </div>
                                                                <div class="col-sm-8 col-7">
                                                                    <p class="">PVN 21% : </p>
                                                                </div>
                                                                <div class="col-sm-4 col-5">
                                                                    <p class="">';
                                                                    $tax = (($grandtotal / 100) *21) ;
                                                                    echo number_format($tax, 2);
                                                                    echo ' €</p>
                                                                </div>
                                                                <div class="col-sm-8 col-7">
                                                                    <p class=" discount-rate"</p>
                                                                </div>
                                                                <div class="col-sm-4 col-5">
                                                                    <p class=""></p>
                                                                </div>
                                                                <div class="col-sm-8 col-7 grand-total-title">
                                                                    <h4 class="">Kopsumma : </h4>
                                                                </div>
                                                                <div class="col-sm-4 col-5 grand-total-amount">
                                                                    <h4 class="">'.number_format($grandtotal, 2).' €</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div> ';
                                        if ($stmtItems) {
                                            mysqli_stmt_close($stmtItems);
                                        } else if ($stmtMenu) {
                                            mysqli_stmt_close($stmtMenu);
                                        } else  if ($stmtOrder) {
                                            mysqli_stmt_close($stmtOrder);
                                        }
                                        mysqli_close($conn);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Zīds © 2020</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Veidots Latgalē <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    <!-- END MAIN CONTAINER -->
</div>


<script src="/assets/js/apps/invoice.js"></script>
<?php
require 'footer.php';
?>