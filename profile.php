<?php 
require "header.php";
?>

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
  <div class="layout-px-spacing">
    <div class="page-header">
      <div class="page-title">
        <h3>Profils</h3>
      </div>
    </div>

    <div class="row layout-spacing">
      <!-- Content -->
      <div class="col-xl-4 col-lg-6 col-md-5 col-sm-12 layout-top-spacing">
        <div class="user-profile layout-spacing">
          <div class="widget-content widget-content-area">
            <div class="d-flex justify-content-between">
              <h3 class="">Informācija</h3>
              <a class="mt-2 edit-profile">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  class="feather feather-user"
                >
                  <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                  <circle cx="12" cy="7" r="4"></circle>
                </svg>
              </a>
            </div>
            <div class="text-center user-info">
                <?php 
                if (isset($_SESSION['avatar'])) {
                    echo '<img src="/assets/img/profile/'.$_SESSION['avatar'].'" alt="avatar" height="90" width="auto" />';
                } else {
                    echo '<img src="/assets/img/profile/default.png" alt="avatar" height="90" width="auto" />';
                }

                echo '<p class="">'.$_SESSION['first_name'].' '.$_SESSION['last_name'].'</p>';
                ?>
            </div>
            <div class="user-info-list">
              <div class="">
                <ul class="contacts-block text-center list-unstyled">
                  <li class="contacts-block__item">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      class="feather feather-coffee"
                    >
                      <path d="M18 8h1a4 4 0 0 1 0 8h-1"></path>
                      <path
                        d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z"
                      ></path>
                      <line x1="6" y1="1" x2="6" y2="4"></line>
                      <line x1="10" y1="1" x2="10" y2="4"></line>
                      <line x1="14" y1="1" x2="14" y2="4"></line>
                    </svg>
                    <?php
                        if (isset($_SESSION['administrator'])) {
                            echo 'Administrators';
                        } else {
                            echo 'Darbinieks';
                        }
                    ?>
                  </li>
                  <li class="contacts-block__item">
                    <?php
                        if (isset($_SESSION['email'])) {
                            echo '<a href="mailto:'.$_SESSION['email'].'">';
                        }
                    ?>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail" >
                            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                            <polyline points="22,6 12,13 2,6"></polyline>
                        </svg>
                      <?php if (isset($_SESSION['email'])) { echo $_SESSION['email'];}?>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-wrapper">
    <div class="footer-section f-section-1">
      <p class="">Zīds POS © 2020</p>
    </div>
    <div class="footer-section f-section-2">
      <p class="">
        Izveidots ar
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          stroke="currentColor"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
          class="feather feather-heart"
        >
          <path
            d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"
          ></path>
        </svg>
      </p>
    </div>
  </div>
</div>
<!--  END CONTENT AREA  -->


<?php 
require("footer.php");
?>