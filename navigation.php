<!--  BEGIN NAVBAR  -->
<div class="header-container fixed-top">
    <header class="header navbar navbar-expand-sm">
        <ul class="navbar-item flex-row">
            <li class="nav-item theme-logo">
                <a href="/">
                    <img src="/assets/img/logo.png" class="navbar-logo" alt="logo" />
                </a>
            </li>
        </ul>

        <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-list">
                <line x1="8" y1="6" x2="21" y2="6"></line>
                <line x1="8" y1="12" x2="21" y2="12"></line>
                <line x1="8" y1="18" x2="21" y2="18"></line>
                <line x1="3" y1="6" x2="3" y2="6"></line>
                <line x1="3" y1="12" x2="3" y2="12"></line>
                <line x1="3" y1="18" x2="3" y2="18"></line>
            </svg>
        </a>

        <ul class="navbar-item flex-row navbar-dropdown">
            <li class="nav-item dropdown user-profile-dropdown order-lg-0 order-1">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php 
                        if (isset($_SESSION['avatar'])) {
                            echo '<img src="/assets/img/profile/'.$_SESSION['avatar'].'" alt="profile" class="img-fluid" />';
                        } else {
                            echo '<img src="/assets/img/profile/default.png" alt="profile" class="img-fluid" />';
                        }
                    ?>
                </a>
                <div class="dropdown-menu position-absolute animated fadeInUp" aria-labelledby="userProfileDropdown">
                    <div class="user-profile-section">
                        <div class="media mx-auto">
                            <?php 
                            if (isset($_SESSION['avatar'])) {
                                echo '<img src="/assets/img/profile/'.$_SESSION['avatar'].'" class="img-fluid mr-2" alt="avatar" />';
                            } else {
                                echo '<img src="/assets/img/profile/default.png" class="img-fluid mr-2" alt="avatar" />';
                            }
                            ?>
                            
                            <div class="media-body">
                                <?php 
                                echo '<h5>'.$_SESSION['first_name'].' '.$_SESSION['last_name'].'</h5>';
                                    if ($_SESSION['administrator'] == '1') {
                                        echo '<p>Administrators</p>';
                                    } else {
                                        echo '<p>Darbinieks</p>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-item">
                        <a href="/profile.php">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-user">
                                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                <circle cx="12" cy="7" r="4"></circle>
                            </svg>
                            <span>Mans profils</span>
                        </a>
                    </div>
                    <div class="dropdown-item">
                        <a href="/logout.php">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-log-out">
                                <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                <polyline points="16 17 21 12 16 7"></polyline>
                                <line x1="21" y1="12" x2="9" y2="12"></line>
                            </svg>
                            <span>Iziet</span>
                        </a>
                    </div>
                </div>
            </li>
        </ul>
    </header>
</div>
<!--  END NAVBAR  -->