<?php 
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION['user_id'])) {
    header("Location: /login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zīds POS</title>
    <link rel="icon" type="image/x-icon" href="/assets/img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/assets/css/plugins.css" />
    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/structure.css" class="structure" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" />

    
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/forms/switches.css" />
    <link rel="stylesheet" type="text/css" href="/plugins/table/datatable/datatables.css" />
    <link rel="stylesheet" type="text/css" href="/plugins/table/datatable/dt-global_style.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/authentication/form-1.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/forms/theme-checkbox-radio.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    
    <link rel="stylesheet" type="text/css" href="/assets/css/users/user-profile.css" />
    <link rel="stylesheet" type="text/css" href="/plugins/select2/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/apps/invoice.css" />
    <!-- Font Awesome -->
    <link href="/assets/css/all.css" rel="stylesheet" type="text/css" />
    

    <style>
        .navbar .navbar-item.navbar-dropdown {
            margin-left: auto;
        }

        .layout-px-spacing {
            min-height: calc(100vh - 145px) !important;
        }
    </style>
</head>
<body class="sidebar-noneoverflow">
    <?php require("navigation.php");?>
    <!-- BEGIN LOADER -->
    <div id="load_screen">
        <div class="loader">
            <div class="loader-content">
                <div class="spinner-grow align-self-center"></div>
            </div>
        </div>
    </div>
    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">
      <div class="overlay"></div>
      <div class="cs-overlay"></div>
      <div class="search-overlay"></div>
      <!--  BEGIN SIDEBAR  -->
      <?php require("sidebar.php");?>
      <!--  END SIDEBAR  -->

