<?php 
require "header.php";
if (!isset($_SESSION['administrator'])) {
    header("Location: /login.php");
    exit();
}
?>


<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">
        <div class="page-header">
            <div class="page-title">
                <h3> Ēdienkarte </h3>
            </div>
        </div>


        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 layout-spacing">
                <div class="widget-content widget-content-area br-6">

                <?php
                if ($_SESSION['administrator'] == '0') {
                    echo "Nav piekļuves";
                } else { ?>

                    <button type="button" style="float: right;" class="btn btn-primary mb-2 mr-2" data-toggle="modal" data-target="#addMenu">Pievienot</button>
                    <div class="modal fade" id="addMenu" tabindex="-1" role="dialog" aria-labelledby="addMenuLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="addMenuLabel">Pievienot ēdienu</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Aizvērt">
                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </div>
                                
                                <form action="includes/menu.inc.php" method="post">
                                    <div class="modal-body"><?php
                                        if (isset($_GET['error'])) {
                                            switch($_GET['error']) {
                                                case "emptycategory": {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Lūdzu, norādiet ēdiena kategoriju!</button>
                                                    </div>';
                                                    break;
                                                }
                                                case "emptyname": {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Lūdzu, norādiet ēdiena nosaukumu!</button>
                                                    </div>';
                                                    break;
                                                }
                                                case "emptyprice": {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Lūdzu, norādiet ēdiena cenu!</button>
                                                    </div>';
                                                    break;
                                                }
                                                case "sqlerror": {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Radās SQL kļūda! Mēģiniet vēlreiz.</button>
                                                    </div>';
                                                    break;
                                                }
                                                default: {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Radās neparedzēta kļūda, lūdzu, atsvaidziniet lappusi!</button>
                                                    </div>';
                                                    break;
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="form-group">
                                            <label for="menu_name">Ēdiena nosaukums</label>
                                            <input type="text" class="form-control" id="name" name="name" autofocus>
                                        </div> 
                                        <div class="form-group">
                                            <label for="menu_price">Ēdiena cena</label>
                                            <input type="text" class="form-control" id="price" name="price">
                                        </div> 
                                        <div class="form-group">
                                        <label for="category_name">Kategorija</label>
                                            <select class="form-control" name="category_id">
                                            <?php
                                                
                                                require 'includes/db.inc.php';
                                                //Get data from mysql
                                                $sql = "SELECT * FROM categories";
                                                $result = mysqli_query($conn, $sql);
                                                if($result) {
                                                    $rows = mysqli_num_rows($result);

                                                    for ($i = 0; $i < $rows; ++$i) {
                                                        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                                                        echo '<option id="'.$row['id'].'" name="'.$row['id'].'">'.$row['name'].'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Atcelt</button>
                                        <button type="submit" name="menu-submit" class="btn btn-primary">Saglabāt</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive mb-4 mt-4">
                        <table id="zero-config" class="table table-hover" style="width:100%;">
                            <thead>
                                <tr>
                                    <th scope="col" class="d-none d-md-table-cell">ID</th>
                                    <th scope="col">Nosaukums</th>
                                    <th scope="col">Cena</th>
                                    <th scope="col">Kategorija</th>
                                    <th scope="col" class="no-content"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                
                                require 'includes/db.inc.php';
                                //Get data from mysql
                                $sql = "SELECT * FROM menu";
                                $stmt = mysqli_stmt_init($conn);

                                if (!mysqli_stmt_prepare($stmt, $sql)) {
                                    header("Location: /menu.php?error=sqlerror");
                                    exit();
                                } else {
                                    mysqli_stmt_execute($stmt);

                                    $result = mysqli_stmt_get_result($stmt);
                                    if($result) {
                                        $rows = mysqli_num_rows($result);
    
                                        for ($i = 0; $i < $rows; ++$i) {
                                            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

                                            $catSQL = "SELECT * FROM categories WHERE id=?";
                                            $catSTMT = mysqli_stmt_init($conn); 
                                            if (!mysqli_stmt_prepare($catSTMT, $catSQL)) {
                                                header("Location: /menu.php?error=sqlerror");
                                                exit();
                                            } else {
                                                mysqli_stmt_bind_param($catSTMT, "i", $row['category_id']);
                                                mysqli_stmt_execute($catSTMT);

                                                $catResult = mysqli_stmt_get_result($catSTMT);
                                                if($catResult) {   
                                                    $catRow = mysqli_fetch_array($catResult, MYSQLI_ASSOC);
                                                    $category = $catRow['name'];
                                                }
                                            }

                                                echo '<div class="modal fade" id="editMenu-'.$row['id'].'" tabindex="-1" role="dialog" aria-labelledby="editMenuLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="editMenuLabel">Labot ēdienu</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Aizvērt">
                                                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                                    </button>
                                                                </div>
                                                        
                                                                <form action="includes/menu.inc.php" method="post">
                                                                    <div class="modal-body">';
                                                                        if (isset($_GET['error'])) {
                                                                            if ($_GET['error'] == "emptyname") {
                                                                                echo '<div class="alert alert-danger mb-4" role="alert">
                                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                                                <strong>Kļūda!</strong> Lūdzu, norādiet ēdiena nosaukumu!</button>
                                                                            </div>';
                                                                            }
                                                                            if ($_GET['error'] == "sqlerror") {
                                                                                echo '<div class="alert alert-danger mb-4" role="alert">
                                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                                                <strong>Kļūda!</strong> Radās SQL kļūda! Mēģiniet vēlreiz.</button>
                                                                            </div>';
                                                                            }
                                                                        }
                                                                        
                                                echo                    '<div class="form-group">
                                                                            <label for="menu_name">Ēdiena nosaukums</label>
                                                                            <input type="hidden" class="form-control" id="id-'.$row['id'].'" name="id" value="'.$row['id'].'">
                                                                            <input type="text" class="form-control" id="name-'.$row['id'].'" name="name" value="'.$row['name'].'">
                                                                        </div> 
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Atcelt</button>
                                                                        <button type="submit" name="menu-edit" class="btn btn-primary">Saglabāt</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>';
                                                echo '<tr>';
                                                echo '<td scope="row" class="d-none d-md-table-cell">'.$row['id'].'</td>';
                                                echo '<td scope="row">'.$row['name'].'</td>';
                                                echo '<td scope="row">'.$row['price'].'</td>';
                                                echo '<td scope="row">'.$category.'</td>';
                                                echo '
                                                <td scope="row">
                                                    <a href="#" data-toggle="modal" data-target="#editMenu-'.$row['id'].'">
                                                        <svg
                                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                            class="feather feather-x-circle table-cancel css-i6dzq1">
                                                            <path
                                                                d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z">
                                                            </path>
                                                        </svg>
                                                    </a>
                                                    <a id="'.$row['id'].'" href="/includes/delete.inc.php?type=menu&id='.$row['id'].'" class="confirmation">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                            class="feather feather-x-circle table-cancel">
                                                            <circle cx="12" cy="12" r="10"></circle>
                                                            <line x1="15" y1="9" x2="9" y2="15"></line>
                                                            <line x1="9" y1="9" x2="15" y2="15"></line>
                                                        </svg>
                                                    </a>
                                                </td>';
                                                echo '</tr>';
                                        }
                                    }
                                }
                                mysqli_stmt_close($stmt);
                                mysqli_close($conn);
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } ?>
                </div>     
            </div>
        </div>
    </div>
    <div class="footer-wrapper">
        <div class="footer-section f-section-1">
            <p class="">
                Zīds © 2020
            </p>
        </div>
        <div class="footer-section f-section-2">
            <p class="">
                Veidots Latgalē
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-heart">
                    <path
                        d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                    </path>
                </svg>
            </p>
        </div>
    </div>
</div>
    <!--  END CONTENT AREA  -->
</div>
<script type="text/javascript">
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Vai esat pārliecināts, ka vēlaties izdzēst šo ēdienu?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
</script>
<?php
require 'footer.php';
?>