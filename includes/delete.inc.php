<?php
require_once 'db.inc.php';

if (isset($_GET['type'])) {
    switch($_GET['type']) {
        case "category": {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                var_dump($id);
                exit();
                $sql = "DELETE FROM categories WHERE id=?";
                mysqli_stmt_bind_param($stmt, "n", $id);
                mysqli_stmt_execute($stmt);

                mysqli_stmt_close($stmt);
                mysqli_close($conn);
                header("Location: /categories.php?delete=success");
            }
            break;
        }
        case "menu": {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $sql = "DELETE FROM menu WHERE id=?";
                mysqli_stmt_bind_param($stmt, "n", $id);
                mysqli_stmt_execute($stmt);

                mysqli_stmt_close($stmt);
                mysqli_close($conn);
                header("Location: /menu.php?delete=success");
            }
            break;
        }
        case "orders": {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $sql = "DELETE FROM orders WHERE id=?";
                mysqli_stmt_bind_param($stmt, "n", $id);
                mysqli_stmt_execute($stmt);

                mysqli_stmt_close($stmt);
                mysqli_close($conn);
                header("Location: /orders.php?delete=success");
            }
            break;
        }
        default:{
            header("Location: /login.php");
            exit();
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
} else {
    header("Location: /login.php");
    exit();
}