<?php

if (isset($_POST['category-submit'])) {
    require 'db.inc.php';

    $name = $_POST['name'];

    if (empty($name)) {
        header("Location: /categories.php?error=emptyname");
        exit();
    } else {
        $sql = "INSERT INTO categories (name) VALUES (?);";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: /categories.php?error=sqlerror");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "s", $name);
            mysqli_stmt_execute($stmt);
    
            header("Location: /categories.php?add=success");
            exit();
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
} else if (isset($_POST['category-edit'])) {
    require 'db.inc.php';

    $id = $_POST['id'];
    $name = $_POST['name'];

    if (empty($name)) {
        header("Location: /categories.php?error=emptyname");
        exit();
    } else {
        $sql = "UPDATE categories SET name=? WHERE id=?";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: /categories.php?error=sqlerror");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "si", $name, $id);
            mysqli_stmt_execute($stmt);
    
            header("Location: /categories.php?edit=success");
            exit();
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
} else {
    header("Location: /categories.php");
    exit();
}


