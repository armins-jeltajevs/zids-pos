<?php
//POST metodes saņemšana no pogas ar name='order-submit'
if (isset($_POST['order-submit'])){

    require_once 'db.inc.php';

    //Mainīgie
    $table = $_POST['table'];
    $item = $_POST['item'];
    $amount = $_POST['amount'];
    $user_id = $_POST['user_id'];
    $state = $_POST['state'];
    $createdAt = $_POST['createdAt'];
    $updatedAt = $_POST['updatedAt'];

    //Pārbaude vai atsūtītie dati nav tukši
    if (empty($table) || empty($item) || empty($amount) || empty($user_id) || empty($state) || empty($createdAt) || empty($updatedAt)) {
        header("Location: /orders.php?error=emptyfields&table=".$table."&user_id=".$user_id);
        exit();
    } else {
        //Pasūtījuma izveidošana
        $sqlCreateOrder = "INSERT INTO orders (`table`,`user_id`,`state`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?);";
        $stmtCreateOrder = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmtCreateOrder, $sqlCreateOrder)) {
            header("Location: /orders.php?error=sqlerror");
            exit();
        } else {
            //Pasūtījuma izveide
            mysqli_stmt_bind_param($stmtCreateOrder, "sisss", $table, $user_id, $state, $createdAt, $updatedAt);
            mysqli_stmt_execute($stmtCreateOrder);

            //Izveidotā pasūtījuma datu ieguve
            $sqlGetOrder = "SELECT * FROM orders WHERE createdAt=?";
            $stmtGetOrder = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($stmtGetOrder, $sqlGetOrder)) {
                header("Location: /orders.php?error=sqlerror");
                exit();
            } else {
                mysqli_stmt_bind_param($stmtGetOrder, "s",$createdAt);
                mysqli_stmt_execute($stmtGetOrder);
                $resultGetOrder = mysqli_stmt_get_result($stmtGetOrder);
                $rowGetOrder = mysqli_fetch_assoc($resultGetOrder); //Iegūst jaunizveidotā orders ierakstu

                //Norādīto ēdienu skaits
                $size = sizeof($item);

                for ($i = 0; $i < $size; ++$i) {
                    //Ēdiena piesaistīšana pasūtījumam
                    $sqlCreateOrderItem = "INSERT INTO order_items (`order_id`,`menu_id`,`amount`) VALUES (?,?,?);";
                    $stmtCreateOrderItem = mysqli_stmt_init($conn);

                    if (!mysqli_stmt_prepare($stmtCreateOrderItem, $sqlCreateOrderItem)) {
                        header("Location: /orders.php?error=sqlerror");
                        exit();
                    } else {
                        //Katram item veidojas order_item ieraksts
                        $sqlMenu = 'SELECT * FROM menu WHERE name="'.$item[$i].'"';
                        $resultMenu = mysqli_query($conn, $sqlMenu);
                        if ($resultMenu) {
                            $rowMenu = mysqli_fetch_array($resultMenu, MYSQLI_ASSOC);

                            mysqli_stmt_bind_param($stmtCreateOrderItem, "iii", $rowGetOrder['id'], $rowMenu['id'], $amount[$i]);
                            mysqli_stmt_execute($stmtCreateOrderItem);
                        }
                    }
                }
                header("Location: /orders.php?added=success");
                exit();
            }
        }
    }
    mysqli_stmt_close($stmtCreateOrder);
    mysqli_stmt_close($stmtOrder);
    mysqli_stmt_close($stmtCreateOrderItem);//Inicializācijas aizvēršana
    mysqli_close($conn);
} else {
    header("Location: /login.php");
    exit();
}