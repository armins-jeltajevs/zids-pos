<?php

if (isset($_POST['login-submit'])) {
    require 'db.inc.php';

    $username = $_POST['username'];
    $password = $_POST['password'];

    if (empty($username) || empty($password)) {
        header("Location: /login.php?error=emptyfields");
        exit();
    } else {
        $sql = "SELECT * FROM users WHERE username=?;"; //SQL pieprasījums
        $stmt = mysqli_stmt_init($conn); //Inicializācija

        if (!mysqli_stmt_prepare($stmt, $sql)){//Sagatavošana 
            header("Location: /login.php?error=sqlerror");//Kļūda ja nav sagatavots
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "s", $username);// ? zīmes aizvietošana ar username
            mysqli_stmt_execute($stmt); //SQL izpildīšana
            $result = mysqli_stmt_get_result($stmt); //Rezultāta iegūšana

            if ($row = mysqli_fetch_assoc($result)) { //Rezultāta sadalīšana rindās (te gaidīta tik 1 atbilde)
                $password_check = password_verify($password, $row['password']); //Paroles pārbaude ar db
                if ($password_check == false) {
                    header("Location: /login.php?error=wrongpass");//Kļūda ja parole ir nepareiza
                    exit();
                } else if ($password_check == true) {
                    session_start();//Sessijas sākšana
                    //Mainīgo saglabāšana sessijā
                    $_SESSION['user_id'] = $row['id'];
                    $_SESSION['username'] = $row['username'];
                    $_SESSION['email'] = $row['email'];
                    $_SESSION['first_name'] = $row['first_name'];
                    $_SESSION['last_name'] = $row['last_name'];
                    $_SESSION['avatar'] = $row['avatar'];
                    $_SESSION['administrator'] = $row['administrator'];

                    //Novirze uz paneli
                    header("Location: /orders.php?login=success");
                    exit();
                } else {
                    //Novirze uz nepareizu paroli ja $password_check nav nedz false nedz true
                    header("Location: /login.php?error=wrongpass");
                    exit();
                }
            } else {
                //Kļūda ja nav lietotāja
                header("Location: /login.php?error=nouser");
                exit();
            }
        }
    }
    mysqli_stmt_close($stmt);//Inicializācijas aizvēršana
    mysqli_close($conn);//DB savienojuma aizvēršana

} else {
    //Novirze ja pierasījums nav no login-submit pogas
    header("Location: /login.php");
    exit();
}