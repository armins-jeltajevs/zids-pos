<?php

if (isset($_POST['menu-submit'])) {
    require 'db.inc.php';

    $name = $_POST['name'];
    $price = str_replace(",",".",$_POST['price']);
    $category = $_POST['category_id'];
    

    if (empty($name)) {
        header("Location: /menu.php?error=emptyname");
        exit();
    } else if (empty($price)) {
        header("Location: /menu.php?error=emptyprice");
        exit();
    } else if (empty($category)) {
        header("Location: /menu.php?error=emptycategory");
        exit();
    } else {
        $sql = "SELECT * FROM categories WHERE name=?";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: /menu.php?error=sqlerror");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "s", $category);
            mysqli_stmt_execute($stmt);

            $result = mysqli_stmt_get_result($stmt);
            if ($row = mysqli_fetch_assoc($result)) {
                $category_id = $row['id'];

                $sql = "INSERT INTO menu (name, price, category_id) VALUES (?,?,?);";
            

                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    header("Location: /menu.php?error=sqlerror");
                    exit();
                } else {
                    mysqli_stmt_bind_param($stmt, "sdi", $name, $price, $category_id);
                    mysqli_stmt_execute($stmt);

                    header("Location: /menu.php?add=success");
                    exit();
                }
            }
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
} else if (isset($_POST['menu-edit'])) {
    require 'db.inc.php';

    $id = $_POST['id'];
    $name = $_POST['name'];

    if (empty($name)) {
        header("Location: /menu.php?error=emptyname");
        exit();
    } else {
        $sql = "UPDATE menu SET name=? WHERE id=?";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: /menu.php?error=sqlerror");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "ss", $name, $id);
            mysqli_stmt_execute($stmt);
    
            header("Location: /menu.php?edit=success");
            exit();
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
} else {
    header("Location: /menu.php");
    exit();
}


