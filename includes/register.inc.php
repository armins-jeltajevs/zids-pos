<?php

if (isset($_POST['register-submit'])){

    require 'db.inc.php';

    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirmPassword = $_POST['confirm-password'];
    $administrator = $_POST['administrator'];

    if (empty($first_name) || empty($last_name) || empty($username) || empty($email) || empty($password) || empty($confirmPassword)) {
        header("Location: /register.php?error=emptyfields&first_name=".$first_name."&last_name=".$last_name."&username=".$username."&email=".$email."&administrator=".$administrator);
        exit();
    } else
    if (!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        header("Location: /register.php?error=invalidemailusername&first_name=".$first_name."&last_name=".$last_name."&administrator=".$administrator);
        exit();
    } else
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header("Location: /register.php?error=invalidemail&first_name=".$first_name."&last_name=".$last_name."&username=".$username."&administrator=".$administrator);
        exit();
    } else
    if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        header("Location: /register.php?error=invalidusername&first_name=".$first_name."&last_name=".$last_name."&email=".$email."&administrator=".$administrator);
        exit();
    } else 
    if ($password !== $confirmPassword) {
        header("Location: /register.php?error=confirmpassword&first_name=".$first_name."&last_name=".$last_name."&username=".$username."&email=".$email."&administrator=".$administrator);
        exit();
    } else if (empty($administrator)) {
        $administrator = false;
    }else {
        $sql = "SELECT username FROM users WHERE username=?";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: /register.php?error=sqlerror");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "s", $username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $result = mysqli_stmt_num_rows($stmt);
            if ($result > 0) {
                header("Location: /register.php?error=usertaken&first_name=".$first_name."&last_name=".$last_name."&email=".$email."&administrator=".$administrator);
                exit();
            } else {
                $sql = "INSERT INTO users (username,email,password,first_name,last_name,administrator) VALUES (?,?,?,?,?,?);";
                $stmt = mysqli_stmt_init($conn);

                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    header("Location: /register.php?error=sqlerror");
                    exit();
                } else {
                    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

                    mysqli_stmt_bind_param($stmt, "sssssi", $username, $email, $hashedPassword, $first_name, $last_name, $administrator);
                    mysqli_stmt_execute($stmt);
            
                    header("Location: /login.php?register=success");
                    exit();
                }
            }
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
} else {
    header("Location: /login.php");
    exit();
}