<?php 
require "header.php";

require_once 'includes/db.inc.php';
?>
        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="page-header">
                    <div class="page-title">
                        <h3> Pasūtījumi </h3>
                    </div>
                </div>


                <div class="row layout-top-spacing" id="cancel-row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 layout-spacing">
                        <div class="widget-content widget-content-area br-6">

                        <!-- Modal -->
                        <button type="button" style="float: right;" class="btn btn-primary mb-2 mr-2" data-toggle="modal" data-target="#addOrder">Pievienot</button>
                        <div class="modal fade" id="addOrder" tabindex="-1" role="dialog" aria-labelledby="addOrderLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <?php
                                    echo '<form action="includes/order.inc.php" method="post">';
                                        echo '<div class="modal-header">';
                                            echo '<h5 class="modal-title" id="menuEditLabel">Pievienot pasūtījumu</h5>';
                                            echo '<button type="button" class="close" data-dismiss="modal" aria-label="Aizvērt">';
                                                echo '<svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>';
                                            echo '</button>';
                                        echo '</div>';
                                        echo '<div class="modal-body">';
                                        if (isset($_GET['error'])) {
                                            switch($_GET['error']) {
                                                case "emptyfields": {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Aizpildiet visus laukus!</button>
                                                    </div>';
                                                    break;  
                                                }
                                                case "emptytable": {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Lūdzu, norādiet galdiņa numuru!</button>
                                                    </div>';
                                                    break;  
                                                }
                                                case "emptyitems": {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Lūdzu, norādiet pasūtītos ēdienus un to daudzumu!</button>
                                                    </div>';
                                                    break;  
                                                }
                                                default: {
                                                    echo '<div class="alert alert-danger mb-4" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Aizvērt"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                                                        <strong>Kļūda!</strong> Radās neparedzēta kļūda, lūdzu, atsvaidziniet lappusi!</button>
                                                    </div>';
                                                    break;
                                                }
                                            }
                                        }
                                            echo '<div class="form-group-table">';
                                                echo '<div class="form-group-table">';
                                                    echo '<div class="form-row mb-4">';
                                                        echo '<label for="table">Galdiņš</label>';
                                                        echo '<input type="number" min="1" max="100" step="1" class="form-control" id="table" name="table">';
                                                        echo '<input type="hidden" min="0" class="form-control" id="user_id" name="user_id" value="'.$_SESSION['user_id'].'">';
                                                    echo '</div>';
                                                    echo '<button class="add-input" id="add-item" style="float: right; border: none; background: none; padding: 0;"><a href="#">+ Pievienot</a></div>';
                                                echo '</div>';

                                                echo '<div class="form-group-table">';
                                                    echo '<label for="menu_item">Ēdiens</label>';
                                                    echo '<div id="itemsContainer">';
                                                        echo '<div class="form-row mb-4" id="1">';
                                                            echo '<div class="col" id="items">';
                                                                echo '<select class="form-control" name="item[]">';
                                                                    
                                                                    $sqlCat = "SELECT * FROM categories";
                                                                    $resultCat = mysqli_query($conn, $sqlCat);
                                                                    if ($resultCat) {
                                                                        $rowsCat = mysqli_num_rows($resultCat);
                                                                        for ($i = 0; $i < $rowsCat; ++$i) {
                                                                            $rowCat = mysqli_fetch_array($resultCat, MYSQLI_ASSOC);
                                                                            $sqlMenu = "SELECT * FROM menu WHERE category_id=?";
                                                                            $sqlMenu = str_replace("?", $rowCat['id'], $sqlMenu);
                                                                            $resultMenu = mysqli_query($conn, $sqlMenu);
                                                                            if ($resultMenu) {
                                                                                $rowsMenu = mysqli_num_rows($resultMenu);
                                                                                echo '<optgroup label="'.$rowCat['name'].'">';
                                                                                for($j = 0; $j < $rowsMenu; ++$j) {
                                                                                    $rowMenu = mysqli_fetch_array($resultMenu, MYSQLI_ASSOC);
                                                                                    echo '<option id="'.$rowMenu['id'].'">'.$rowMenu['name'].'</option>';
                                                                                }
                                                                            }
                                                                            echo '</optgroup>';
                                                                        }
                                                                        
                                                                    }
                                                                echo '</select>';
                                                            echo '</div>';
                                                            echo '<div class="col" id="itemsAmount">';
                                                                echo '<input type="number" id="menu_amount" name="amount[]" class="form-control" placeholder="Daudzums" min="0">';
                                                            echo '</div>';
                                                        echo '</div>';
                                                    echo '</div>';
                                                                
                                                echo '</div>';
                                            echo '<div class="form-group-table">';
                                                echo '<input type="hidden" class="form-control" id="createdAt" name="createdAt" value="'.date("Y-m-d") .' '. date("H:i:s").'">';
                                                echo '<input type="hidden" class="form-control" id="updatedAt" name="updatedAt" value="'.date("Y-m-d") .' '. date("H:i:s").'">';
                                                echo '<label for="state_item">Statuss</label>';
                                                    echo '<div class="form-row mb-4">';
                                                        echo '<select class="form-control mb-4" name="state">';
                                                            echo '<option value="Pasūtīts">Pasūtīts</option>';
                                                            echo '<option value="Pasniegts">Pasniegts</option>';
                                                            echo '<option value="Apmaksāts">Apmaksāts</option>';
                                                            echo '<option value="Atmaksāts">Atmaksāts</option>';
                                                            echo '<option value="Atcelts">Atcelts</option>';
                                                        echo '</select>';
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '<div class="modal-footer">';
                                            echo '<button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Atcelt</button>';
                                            echo '<button type="submit" name="order-submit" class="btn btn-primary">Saglabāt</button>';
                                        echo '</div>';
                                    echo '</form>';
                                        
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mb-4 mt-4">
                            <table id="zero-config" class="table table-hover" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th scope="col">Galds</th>
                                        <th scope="col">Viesmīlis</th>
                                        <th scope="col" class="d-none d-md-table-cell">Laiks</th>
                                        <th scope="col" class="d-none d-sm-table-cell">Stāvoklis</th>
                                        <th scope="col" class="no-content"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    //Get data from mysql
                                    $sql = "SELECT * FROM orders";
                                    $stmt = mysqli_stmt_init($conn);

                                    if (!mysqli_stmt_prepare($stmt, $sql)) {
                                        header("Location: /orders.php?error=sqlerror");
                                        exit();
                                    } else {
                                        mysqli_stmt_execute($stmt);
                                        $result = mysqli_stmt_get_result($stmt);
                                        if($result) {
                                            $rows = mysqli_num_rows($result);

    
                                            for ($i = 0; $i < $rows; ++$i) {
                                                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                                                echo '<tr>';
                                                    echo '<td scope="row">Nr. '.$row['table'].'</td>';

                                                    $sqlUser = "SELECT * FROM users WHERE id=?";
                                                    $stmtUser = mysqli_stmt_init($conn);

                                                    if (!mysqli_stmt_prepare($stmtUser, $sqlUser)) {
                                                        header("Location: /orders.php?error=sqlerror");
                                                        exit();
                                                    } else {
                                                        mysqli_stmt_bind_param($stmtUser, "i", $row['user_id']);
                                                        mysqli_stmt_execute($stmtUser);
                                                        $resultUser = mysqli_stmt_get_result($stmtUser);
                                                        if ($resultUser) {
                                                            $rowUser = mysqli_fetch_array($resultUser, MYSQLI_ASSOC);
                                                            $user = $rowUser['first_name'].' '.$rowUser['last_name'];
                                                        }
                                                        
                                                    }
                                                    echo '<td scope="row">'.$user.'</td>';


                                                    echo '<td scope="row" class="d-none d-md-table-cell">';
                                                    $date = new DateTime($row['updatedAt']);
                                                    echo $date->format("d.m.Y H:i");
                                                    echo '</td>';
                                                    echo '<td scope="row" class="d-none d-sm-table-cell">';
                                                        switch($row['state']){
                                                            case "Pasniegts": {
                                                                echo '<span class="shadow-none badge badge-success">Pasniegts</span>';
                                                                break;
                                                            }
                                                            case "Apmaksāts": {
                                                                echo '<span class="shadow-none badge badge-secondary">Apmaksāts</span>';
                                                                break;
                                                            }
                                                            case "Atmaksāts": {
                                                                echo '<span class="shadow-none badge badge-warning">Atmaksāts</span>';
                                                                break;
                                                            }
                                                            case "Atcelts": {
                                                                echo '<span class="shadow-none badge badge-danger">Atcelts</span>';
                                                                break;
                                                            }
                                                            default: {
                                                                echo '<span class="shadow-none badge badge-primary">Pasūtīts</span>';
                                                                break;
                                                            }
                                                        }
                                                    echo '</td>';
                                                    echo '<td scope="row">';
                                                        echo '<a href="/order-view?order_id='.$row['id'].'">';
                                                        
                                                            echo '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel">';
                                                                echo '<circle cx="11" cy="11" r="8"></circle>';
                                                                echo '<line x1="21" y1="21" x2="16.65" y2="16.65"></line>';
                                                            echo '</svg>';
                                                        echo '</a>';

                                                        echo '<a href="#edit">';
                                                            echo '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel">';
                                                                echo '<path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>';
                                                            echo '</svg>';
                                                        echo '</a>';

                                                        echo '<a id="'.$row['id'].'" href="/includes/delete.inc.php?type=orders&id='.$row['id'].'" class="confirmation">';
                                                            echo '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel">';
                                                                echo '<circle cx="12" cy="12" r="10"></circle>';
                                                                echo '<line x1="15" y1="9" x2="9" y2="15"></line>';
                                                                echo '<line x1="9" y1="9" x2="15" y2="15"></line>';
                                                            echo '</svg>';
                                                        echo '</a>';
                                                    echo '</td>';
                                                echo '</tr>';
                                            }
                                        }
                                    }
                                    mysqli_stmt_close($stmt);
                                    mysqli_stmt_close($stmtUser);
                                    mysqli_close($conn);
                                ?>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">
                        Zīds © 2020
                    </p>
                </div>
                <div class="footer-section f-section-2">
                    <p class="">
                        Veidots Latgalē
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                            class="feather feather-heart">
                            <path
                                d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                            </path>
                        </svg>
                    </p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".add-input").click(function(){
        var items = document.getElementById("itemsContainer");
        var list = document.getElementById("1");
        var clone = list.cloneNode(true);
        clone.id = parseInt(items.lastChild.id)+1
        items.appendChild(clone);
    });
});
</script>
<script src="/plugins/select2/select2.min.js"></script>
<?php
require 'footer.php';
?>